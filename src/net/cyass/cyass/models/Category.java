package net.cyass.cyass.models;

public class Category {

	private String objectId;
	private String categoryName;
	private String categoryHint;

	public Category (String objectId, String categoryName, String categoryHint) {
		this.objectId = objectId;
		this.categoryName = categoryName;
		this.categoryHint = categoryHint;
	}

	public String getObjectId() {
		return objectId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public String getCategoryHint() {
		return categoryHint;
	}
}
