package net.cyass.cyass.models;


public class Message {
	private String objectId;
	private String userName;
	private String userId;
	private String title;
	private String content;
	private int listenNumber;

	public Message(String objectId, String userId, String userName, String title, String content, int listenNumber) {
		super();
		this.objectId = objectId;
		this.userId = userId;
		this.userName = userName;
		this.title = title;
		this.content = content;
		this.listenNumber = listenNumber;
	}

	@Override
	public String toString() {
		return userName;// filterに使用する文字列
	}

	public String getObjectId() {
		return objectId;
	}

	public String getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public int getListenNumber() {
		return listenNumber;
	}
}
