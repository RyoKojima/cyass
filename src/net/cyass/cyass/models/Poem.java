package net.cyass.cyass.models;


public class Poem {
	private String objectId;
	private String userName;
	private String title;
	private String content;

	public Poem(String objectId, String userName, String title, String content) {
		super();
		this.objectId = objectId;
		this.userName = userName;
		this.title = title;
		this.content = content;
	}

	@Override
	public String toString() {
		return userName;// filterに使用する文字列
	}

	public String getObjectId() {
		return objectId;
	}

	public String getUserName() {
		return userName;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}
}
