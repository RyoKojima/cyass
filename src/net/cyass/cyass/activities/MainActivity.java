package net.cyass.cyass.activities;

import net.cyass.cyass.R;
import net.cyass.cyass.fragments.CategoryFragment;
import net.cyass.cyass.fragments.MessageListFragment;
import net.cyass.cyass.utils.MainTabListener;
import net.cyass.cyass.utils.NetWorkCheck;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.parse.ParseInstallation;
import com.parse.ParseUser;

public class MainActivity extends Activity {

	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //通信処理がある前にif文などで
        if ( NetWorkCheck.netWorkCheck(this.getApplicationContext()) ){
        	setContentView(R.layout.activity_main);
            ParseInstallation.getCurrentInstallation().saveInBackground();

        } else {
        	setContentView(R.layout.activity_error);
        	return;
        }

        // アクションバーを取得してモードをタブモードへセット
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // フラグメントを生成してTabへセット
        Fragment fragment1 = new MessageListFragment();
        // 新しく生成したTabインスタンスには、タイトル文字列、アイコン、リスナーをセットすることができる
        ActionBar.Tab tab1 = actionBar.newTab();
        tab1.setText("ホーム");
        tab1.setTabListener(new MainTabListener(fragment1));
        actionBar.addTab(tab1);

        Fragment fragmet2 = new CategoryFragment();
        actionBar.addTab(actionBar.newTab().setText("見つける")
                .setTabListener(new MainTabListener(fragmet2)));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);

	    return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

	    int id = item.getItemId();

		if (id == R.id.action_logaout) {
			ParseUser.logOut();

        	Intent i = new Intent(this, LandingActivity.class);
        	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        	startActivity(i);
        	finish();
		}

		if(id == R.id.action_profile) {
			Intent i = new Intent(this, net.cyass.cyass.activities.ProfileActivity.class);
			startActivity(i);
		}

		return super.onOptionsItemSelected(item);
	}

	//戻るボタンを押した時にログイン前のアクティビティに戻れないようにする。
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	moveTaskToBack(true);
        }
        return false;
    }
}
