package net.cyass.cyass.activities;

import net.cyass.cyass.R;
import net.cyass.cyass.utils.NetWorkCheck;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class DetailActivity extends Activity {

	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_progress);
		TextView load_text = (TextView)findViewById(R.id.load_text);
		load_text.setText("受信中...");

		// adView を作成する
        adView = new AdView(this);
        adView.setAdUnitId("ca-app-pub-6248511118429340/5954918511");
        adView.setAdSize(AdSize.BANNER);
	}

	@Override
	protected void onResume() {
		super.onResume();
		adView.resume();

		final String messageId = (String)getIntent().getStringExtra("messageId");

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Message");
		query.include("userId");
		query.whereEqualTo("objectId", messageId);
		query.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(final ParseObject message, ParseException e) {
				if(message == null) {

					setContentView(R.layout.activity_no_detail);

				} else {
					setContentView(R.layout.activity_detail);

					ParseUser msgUser = message.getParseUser("userId");
					TextView view_sum_text = (TextView)findViewById(R.id.view_sum_text);
					TextView title_text = (TextView)findViewById(R.id.title_text);
					TextView content_text = (TextView)findViewById(R.id.content_text);
					TextView user_name_text = (TextView)findViewById(R.id.user_name_text);

					view_sum_text.setText(String.valueOf(message.getInt("listenNumber")+" VIEW"));
					title_text.setText(message.getString("title"));
					content_text.setText(message.getString("content"));
					user_name_text.setText("from: "+msgUser.getUsername());

                    //クラスが存在していたら上書きする。
					int listenNumber = message.getInt("listenNumber") + 1;

					message.put("objectId", message.getObjectId());
					message.put("listenNumber", listenNumber);
					message.saveInBackground();
				}

				//広告を追加する。
				LinearLayout adLayout = (LinearLayout)findViewById(R.id.layout_ad);

				adLayout.addView(adView);

			    AdRequest adRequest = new AdRequest.Builder()
			    .addTestDevice("2A72FAA9B64295B3D3B6B70FEE056EAC").build();

			    adView.loadAd(adRequest);
			}

		});
	}

	@Override
	protected void onStop() {
		super.onStop();
        //再開後、親レイアウトに同じViewを二度追加しないために、広告のViewをRmoveしておく。
		if ( NetWorkCheck.netWorkCheck(this.getApplicationContext()) ){
			LinearLayout adLayout = (LinearLayout)findViewById(R.id.layout_ad);
			adLayout.removeView(adView);
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);

	    return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

	    int id = item.getItemId();

		if (id == R.id.action_logaout) {
			ParseUser.logOut();

        	Intent i = new Intent(this, LandingActivity.class);
        	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        	startActivity(i);
        	finish();
		}

		if(id == R.id.action_profile) {
			Intent i = new Intent(this, net.cyass.cyass.activities.ProfileActivity.class);
			startActivity(i);
		}

		if(id == android.R.id.home) {
			Intent intent = new Intent(this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}

		return super.onOptionsItemSelected(item);
	}

}
