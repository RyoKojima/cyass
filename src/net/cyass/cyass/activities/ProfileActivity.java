package net.cyass.cyass.activities;

import net.cyass.cyass.R;
import net.cyass.cyass.utils.NetWorkCheck;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class ProfileActivity extends Activity {

	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setTitle(R.string.profile_title);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_progress);

		// adView を作成する
        adView = new AdView(this);
        adView.setAdUnitId("ca-app-pub-6248511118429340/5954918511");
        adView.setAdSize(AdSize.BANNER);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if ( NetWorkCheck.netWorkCheck(this.getApplicationContext()) == false){
			setContentView(R.layout.activity_error);
			return;
        }

		adView.resume();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Message");
		query.include("userId");
		query.whereEqualTo("userId", ParseUser.getCurrentUser());
		query.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject message, ParseException e) {

				if(message == null) {
					openActivity(false);
					return;

				} else {
					openActivity(true);

					String listenNumber = String.valueOf(message.getInt("listenNumber"));

					TextView title_text = (TextView)findViewById(R.id.title_text);
					TextView listen_number_text = (TextView)findViewById(R.id.listen_number_text);
					TextView content_text = (TextView)findViewById(R.id.content_text);

					title_text.setText(message.getString("title"));
					listen_number_text.setText(listenNumber+" VIEW");
					content_text.setText(message.getString("content"));
				}
			}
		});

	}

	@Override
	protected void onStop() {
		super.onStop();
        //再開後、親レイアウトに同じViewを二度追加しないために、広告のViewをRmoveしておく。
		if ( NetWorkCheck.netWorkCheck(this.getApplicationContext()) ){
			LinearLayout adLayout = (LinearLayout)findViewById(R.id.layout_ad);
			adLayout.removeView(adView);
        }
	}

	public void openActivity(boolean flag) {
		if (flag == false) {
			setContentView(R.layout.activity_no_wish);
		} else {
			setContentView(R.layout.activity_my_message);
		}

		LinearLayout adLayout = (LinearLayout)findViewById(R.id.layout_ad);

		adLayout.addView(adView);

	    AdRequest adRequest = new AdRequest.Builder()
	    .addTestDevice("2A72FAA9B64295B3D3B6B70FEE056EAC").build();

	    adView.loadAd(adRequest);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);

	    return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

	    int id = item.getItemId();

		if (id == R.id.action_logaout) {
			ParseUser.logOut();

        	Intent i = new Intent(this, LandingActivity.class);
        	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        	startActivity(i);
        	finish();
		}

		if(id == R.id.action_profile) {
			Intent i = new Intent(this, net.cyass.cyass.activities.ProfileActivity.class);
			startActivity(i);
		}

		if(id == android.R.id.home) {
			Intent intent = new Intent(this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}

		return super.onOptionsItemSelected(item);
	}
}
