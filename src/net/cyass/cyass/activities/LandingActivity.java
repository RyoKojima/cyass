package net.cyass.cyass.activities;

import net.cyass.cyass.R;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.parse.ParseUser;

public class LandingActivity extends Activity {

	@Override
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_landing);

		ActionBar actionBar = getActionBar();
		actionBar.hide();
	}

	@Override
	protected void onResume() {
		super.onResume();
		ParseUser currentUser = ParseUser.getCurrentUser();
		if(currentUser != null) {
			Intent i = new Intent(this, net.cyass.cyass.activities.MainActivity.class);
			startActivity(i);
			finish();
		}
	}

	public void callSignUp(View v) {
		Intent i = new Intent(this, net.cyass.cyass.activities.SignInActivity.class);
		startActivity(i);
	}

	public void callLogin(View v) {
		Intent i = new Intent(this, net.cyass.cyass.activities.LoginActivity.class);
		startActivity(i);
	}
}
