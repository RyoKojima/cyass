package net.cyass.cyass.activities;

import java.util.ArrayList;
import java.util.List;

import net.cyass.cyass.R;
import net.cyass.cyass.adapteres.PoemAdapter;
import net.cyass.cyass.models.Poem;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class PoemListActivity extends Activity implements
SwipeRefreshLayout.OnRefreshListener {

	private SwipeRefreshLayout mSwipeRefreshLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_poem_list);

		final String categoryName = (String)getIntent().getStringExtra("categoryName");
		// アクションバーを取得してモードをタブモードへセット
        final ActionBar actionBar = getActionBar();
        actionBar.setTitle(categoryName);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_layout);

		// 色設定
        mSwipeRefreshLayout.setColorSchemeResources(R.color.refresh_color1,
       		R.color.refresh_color2, R.color.refresh_color3,
               R.color.refresh_color4);
        // Listenerをセット
        mSwipeRefreshLayout.setOnRefreshListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();

		final String categoryId = (String)getIntent().getStringExtra("categoryId");

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Poem");
		query.include("userId");
		query.whereEqualTo("categoryId", categoryId);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> poemList, ParseException e) {
				if(e == null) {

					ArrayList<Poem> poem_item = new ArrayList<Poem>();

					for (ParseObject poem : poemList){
						ParseUser creater = poem.getParseUser("userId");
						poem_item.add(new Poem(poem.getObjectId(),
							creater.getUsername(), poem.getString("title"),  poem.getString("textContent")));
					}

					PoemAdapter adapter = new PoemAdapter(PoemListActivity.this, 0, poem_item);

					final ListView list = (ListView)findViewById(R.id.poem_list);
					list.setAdapter(adapter);
					list.setDivider(null);
				}
			}
		});
	}

	@Override
	public void onRefresh() {
		// TODO 自動生成されたメソッド・スタブ

		final String categoryId = (String)getIntent().getStringExtra("categoryId");

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Poem");
		query.include("userId");
		query.whereEqualTo("categoryId", categoryId);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> poemList, ParseException e) {
				if(e == null) {

					ArrayList<Poem> poem_item = new ArrayList<Poem>();

					for (ParseObject poem : poemList){
						ParseUser creater = poem.getParseUser("userId");
						poem_item.add(new Poem(poem.getObjectId(),
							creater.getUsername(), poem.getString("title"),  poem.getString("textContent")));
					}

					PoemAdapter adapter = new PoemAdapter(PoemListActivity.this, 0, poem_item);

					final ListView list = (ListView)findViewById(R.id.poem_list);
					list.setAdapter(adapter);
					list.setDivider(null);
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);

	    return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

	    int id = item.getItemId();

		if (id == R.id.action_logaout) {
			ParseUser.logOut();

        	Intent i = new Intent(this, LandingActivity.class);
        	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        	startActivity(i);
        	finish();
		}

		if(id == android.R.id.home) {
			Intent intent = new Intent(this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}

		return super.onOptionsItemSelected(item);
	}
}
