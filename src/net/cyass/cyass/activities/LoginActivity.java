package net.cyass.cyass.activities;

import net.cyass.cyass.R;
import net.cyass.cyass.utils.UserNameFilter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginActivity extends Activity {

	private InputFilter[] filters = { new UserNameFilter() };
	private boolean misClickGuard;

	@Override
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		getActionBar().setTitle(R.string.login);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		EditText login_username_edit = (EditText)findViewById(R.id.login_username_edit);
		login_username_edit.setFilters(filters);
	}

	public void Login(View v) {
		// クリックイベント実行されていれば実行しない
	    if (misClickGuard == true) {
	    	return;
	    }
	    // クリックイベントを禁止する
	    misClickGuard = true;

		EditText login_username_edit = (EditText)findViewById(R.id.login_username_edit);
		EditText login_password_edit = (EditText)findViewById(R.id.login_password_edit);

		String username_str = login_username_edit.getText().toString();
		String password_str = login_password_edit.getText().toString();

		ParseUser.logInInBackground(username_str, password_str, new LogInCallback() {
		    @Override
			public void done(ParseUser user, ParseException e) {

		        if (user != null) {
		        	Intent i = new Intent(LoginActivity.this, net.cyass.cyass.activities.MainActivity.class);
					startActivity(i);
					finish();

		        } else {
		            TextView validation = (TextView)findViewById(R.id.login_validation_text);
					validation.setText(R.string.not_macth);
					misClickGuard = false;
		        }
		    }
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()){
			case android.R.id.home:
				Intent intent = new Intent(this, LandingActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				break;
		}
		return true;
	}

}
