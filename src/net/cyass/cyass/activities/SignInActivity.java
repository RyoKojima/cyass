package net.cyass.cyass.activities;

import net.cyass.cyass.R;
import net.cyass.cyass.utils.UserNameFilter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignInActivity extends Activity {

	private InputFilter[] filters = { new UserNameFilter() };
	private boolean misClickGuard;

	@Override
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);

		getActionBar().setTitle(R.string.signup);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		EditText username_edit = (EditText)findViewById(R.id.username_edit);
		// フィルターの配列をセット
		username_edit.setFilters(filters);
	}

	public void btnSignUp_onClick(View view) {
		// クリックイベント実行されていれば実行しない
	    if (misClickGuard == true) {
	    	return;
	    }
	    // クリックイベントを禁止する
	    misClickGuard = true;

		EditText username_edit = (EditText)findViewById(R.id.username_edit);
		EditText password_edit = (EditText)findViewById(R.id.password_edit);

		String username_str = username_edit.getText().toString();
		String password_str = password_edit.getText().toString();

		if(validation(username_str, password_str) == false) {
			misClickGuard = false;
			return;
		} else {
			setContentView(R.layout.activity_progress);
			TextView load_text = (TextView)findViewById(R.id.load_text);
			load_text.setText("送信中...");
		}

        ParseUser user = new ParseUser();
        user.setUsername(username_str);
        user.setPassword(password_str);

        user.signUpInBackground(new SignUpCallback() {
			@Override
			public void done(ParseException e) {
				if(e == null) {
					Intent i = new Intent(SignInActivity.this, net.cyass.cyass.activities.MainActivity.class);
					startActivity(i);
					finish();

				} else {
					setContentView(R.layout.activity_sign_in);
					TextView validation = (TextView)findViewById(R.id.signIn_validation_text);
					validation.setText(R.string.is_unique);
					misClickGuard = false;
				}
			}
        });
	}

	protected boolean validation(String username, String password) {
		if(username.equals("") || password.equals("")){
			TextView validation = (TextView)findViewById(R.id.signIn_validation_text);
			validation.setText(R.string.not_empty);
			return false;
		}

		if(password.length() < 8) {
			TextView validation = (TextView)findViewById(R.id.signIn_validation_text);
			validation.setText(R.string.min_length);
			return false;
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch (item.getItemId()){
			case android.R.id.home:
				Intent intent = new Intent(this, LandingActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				break;
		}
		return true;
	}
}
