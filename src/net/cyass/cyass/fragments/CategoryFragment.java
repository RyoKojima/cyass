package net.cyass.cyass.fragments;

import java.util.ArrayList;
import java.util.List;

import net.cyass.cyass.R;
import net.cyass.cyass.adapteres.CategoryAdapter;
import net.cyass.cyass.models.Category;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class CategoryFragment extends Fragment {

	@Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
            Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_category, container, false);
		return view;
    }

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Category");
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> categoryList, ParseException e) {
				if(e == null) {
					ArrayList<Category> category_item = new ArrayList<Category>();

					for (ParseObject category : categoryList) {
						category_item.add(new Category(category.getObjectId(),
								category.getString("categoryName"),category.getString("categoryHint")));
					}

					CategoryAdapter adapter = new CategoryAdapter(getView().getContext(), 0, category_item);
					final ListView list = (ListView)getView().findViewById(R.id.category_list);

					list.setAdapter(adapter);

					list.setOnItemClickListener(
							new AdapterView.OnItemClickListener() {
								@Override
								public void onItemClick(AdapterView<?> parent,
										View view, int position, long id) {
									ListView listView = (ListView) parent;

					                Category cellData = (Category) listView.getItemAtPosition(position);
					        		Intent i = new Intent(view.getContext(), net.cyass.cyass.activities.PoemListActivity.class);
					        		i.putExtra("categoryId", cellData.getObjectId());
					        		i.putExtra("categoryName", cellData.getCategoryName());
					        		startActivity(i);
								}
							}
					);
				}
			}
		});
	}


}