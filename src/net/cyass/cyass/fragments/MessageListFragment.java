package net.cyass.cyass.fragments;

import java.util.ArrayList;
import java.util.List;

import net.cyass.cyass.R;
import net.cyass.cyass.adapteres.MessageAdapter;
import net.cyass.cyass.models.Message;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class MessageListFragment extends Fragment implements
SwipeRefreshLayout.OnRefreshListener {

	private SwipeRefreshLayout mSwipeRefreshLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
            Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.flagment_message_list, container, false);

		mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_layout);

		 // 色設定
        mSwipeRefreshLayout.setColorSchemeResources(R.color.refresh_color1,
        		R.color.refresh_color2, R.color.refresh_color3,
                R.color.refresh_color4);
        // Listenerをセット
        mSwipeRefreshLayout.setOnRefreshListener(this);

		return view;
    }

	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Message");
		query.include("userId");
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> messageList, ParseException e) {

				if(e == null) {

					ArrayList<Message> message_item = new ArrayList<Message>();

					for (ParseObject message : messageList){
						ParseUser msgUser = message.getParseUser("userId");
						message_item.add(new Message(message.getObjectId(), msgUser.getObjectId(),
							msgUser.getUsername(), message.getString("title"),  message.getString("content"),
							message.getInt("listenNumber")));
					}

					MessageAdapter adapter = new MessageAdapter(getView().getContext(), 0, message_item);

					final ListView list = (ListView)getView().findViewById(R.id.message_list);
					list.setAdapter(adapter);
					list.setTextFilterEnabled(true);
					list.setDivider(null);
					list.addFooterView(LayoutInflater.from(getActivity()).inflate(R.layout.card_footer, list, false));

					/*list.setOnItemClickListener(
							new AdapterView.OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> parent,
										View view, int position, long id) {
									// TODO 自動生成されたメソッド・スタブ

									ListView listView = (ListView) parent;

					                Message cellData = (Message) listView.getItemAtPosition(position);
					        		Intent i = new Intent(view.getContext(), net.cyass.cyass.activities.DetailActivity.class);
					        		i.putExtra("messageId", cellData.getObjectId());
					        		startActivity(i);
								}
							}
					);*/
				}
			}
		});
    }

	@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Fragment内で残しておきたいデータを保存
	}

	@Override
	public void onRefresh() {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Message");
		query.include("userId");
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> messageList, ParseException e) {

				if(e == null) {

					ArrayList<Message> message_item = new ArrayList<Message>();

					for (ParseObject message : messageList){
						ParseUser msgUser = message.getParseUser("userId");
						message_item.add(new Message(message.getObjectId(), msgUser.getObjectId(),
							msgUser.getUsername(), message.getString("title"),  message.getString("content"),
							message.getInt("listenNumber")));
					}

					MessageAdapter adapter = new MessageAdapter(getView().getContext(), 0, message_item);

					final ListView list = (ListView)getView().findViewById(R.id.message_list);
					list.setAdapter(adapter);
					list.setTextFilterEnabled(true);

					/*list.setOnItemClickListener(
							new AdapterView.OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> parent,
										View view, int position, long id) {
									// TODO 自動生成されたメソッド・スタブ

									ListView listView = (ListView) parent;

					                Message cellData = (Message) listView.getItemAtPosition(position);
					        		Intent i = new Intent(view.getContext(), net.cyass.cyass.activities.DetailActivity.class);
					        		i.putExtra("messageId", cellData.getObjectId());
					        		startActivity(i);
								}
							}
					);*/
				}
			}
		});

		new Handler().postDelayed(new Runnable() {
			@Override
	        public void run() {
	        // 更新が終了したらインジケータ非表示
				mSwipeRefreshLayout.setRefreshing(false);
	        }
	    }, 2000);
	}
}
