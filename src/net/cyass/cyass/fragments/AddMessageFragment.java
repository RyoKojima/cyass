package net.cyass.cyass.fragments;

import net.cyass.cyass.R;
import net.cyass.cyass.utils.NetWorkCheck;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class AddMessageFragment extends Fragment {

	private boolean misClickGuard;
	private boolean btnEnabled_1;
	private boolean btnEnabled_2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.flagment_add_message, container, false);
    }

	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final View layout_v = getView();
        final EditText title_edit = (EditText)layout_v.findViewById(R.id.title_edit);
        final EditText send_message_edit = (EditText)layout_v.findViewById(R.id.send_message_edit);
        final TextView count_text1 = (TextView)layout_v.findViewById(R.id.count_text1);
        final TextView count_text2 = (TextView)layout_v.findViewById(R.id.count_text2);
        final Button send_message_btn = (Button)layout_v.findViewById(R.id.send_message_btn);

        title_edit.addTextChangedListener(new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				int textColor = R.color.font_medium_color;
				btnEnabled_1 = false;

				// 入力文字数の表示
		        int txtLength = s.length();
		        count_text1.setText(Integer.toString(txtLength) + "/15");

		        // 指定文字数オーバーで文字色を赤くする
		        if ( txtLength > 15) {
		        	textColor = Color.RED;
		        }

		        if (txtLength <= 15 && txtLength > 0) {
		        	btnEnabled_1 = true;
		        }

		        setBtnEnabled(btnEnabled_1, btnEnabled_2, send_message_btn);

		        count_text1.setTextColor(textColor);
			}

			@Override
			public void afterTextChanged(Editable s) {}
        });

        send_message_edit.addTextChangedListener(new TextWatcher(){

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				int textColor = R.color.font_medium_color;
				btnEnabled_2 = false;

		        int txtLength = s.length();
		        count_text2.setText(Integer.toString(txtLength) + "/31");

		        if ( txtLength > 31) {
		        	textColor = Color.RED;
		        }

		        if (txtLength <= 31 && txtLength > 0) {
		        	btnEnabled_2 = true;
		        }

		        setBtnEnabled(btnEnabled_1, btnEnabled_2, send_message_btn);
		        count_text2.setTextColor(textColor);
			}

			@Override
			public void afterTextChanged(Editable s) {}
        });

        send_message_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View btn_view) {
                //ネットワークに接続していない場合は関イベントを実行しない。
				if ( NetWorkCheck.netWorkCheck(btn_view.getContext()) == false ){
					return;
				}

				// クリックイベント実行されていれば実行しない
			    if (misClickGuard == true) {
			    	return;
			    }
			    // クリックイベントを禁止する
			    misClickGuard = true;

			    final String title_str = title_edit.getText().toString();
				final String content_str = send_message_edit.getText().toString();

				if(msgValidation(title_str, content_str) == false) {
					misClickGuard = false;
					return;
				}

				ParseQuery<ParseObject> query = ParseQuery.getQuery("Message");
				query.whereEqualTo("userId", ParseUser.getCurrentUser());
				query.getFirstInBackground(new GetCallback<ParseObject>() {

					@Override
					public void done(ParseObject message, ParseException e) {
						if (message == null ) {
							//新規作成
							ParseACL messageACL = new ParseACL(ParseUser.getCurrentUser());
							messageACL.setPublicReadAccess(true);
							messageACL.setPublicWriteAccess(true);

							ParseObject messageClass = new ParseObject("Message");
							messageClass.put("userId", ParseUser.getCurrentUser());
							messageClass.put("title", title_str);
							messageClass.put("content", content_str);
							messageClass.put("listenNumber", 0);
							messageClass.setACL(messageACL);
							messageClass.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									Intent i = new Intent(layout_v.getContext(), net.cyass.cyass.activities.MainActivity.class);
								    startActivity(i);
								}
							});

						} else {
							//アップデート
							message.put("userId", ParseUser.getCurrentUser());
							message.put("title", title_str);
							message.put("content", content_str);
							message.put("listenNumber", 0);
							message.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									Intent i = new Intent(layout_v.getContext(), net.cyass.cyass.activities.MainActivity.class);
								    startActivity(i);
								}
							});
						}
					}
				});
			}
		});
	}

	public void setBtnEnabled(boolean enabled_1, boolean enabled_2, Button btn) {
		if (enabled_1 == true && enabled_2 == true) {
			btn.setEnabled(true);
	        btn.setBackgroundResource(R.drawable.customize_selector);
		} else {
			btn.setEnabled(false);
	        btn.setBackgroundResource( R.drawable.btn_enabled);
		}
	}

	public boolean msgValidation(String title, String content) {

		if(title.equals("") || content.equals("")) {
			return false;
		}

		if(title.length() > 15 || content.length() > 31) {
			return false;
		}

		return true;
	}
}
