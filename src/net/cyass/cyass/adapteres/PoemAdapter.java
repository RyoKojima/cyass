package net.cyass.cyass.adapteres;

import java.util.ArrayList;

import net.cyass.cyass.R;
import net.cyass.cyass.models.Poem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PoemAdapter extends ArrayAdapter<Poem> {
	static class ViewHolder {
        TextView titleText;
        TextView poemContent;
        TextView CreaterNameText;
        TextView unitText;
    }

    private LayoutInflater inflater;

    // コンストラクタ
    public PoemAdapter(Context findCallback, int resourceId, ArrayList<Poem> items) {
        super(findCallback,resourceId, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View view = convertView;

        // Viewを再利用している場合は新たにViewを作らない
        if (view == null) {
            inflater =  (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listitem_poem, null);

            TextView title_text = (TextView)view.findViewById(R.id.title_text);
            TextView poem_content_text = (TextView)view.findViewById(R.id.poem_content_text);
            TextView creater_name_text = (TextView)view.findViewById(R.id.creater_name_text);

            holder = new ViewHolder();
            holder.titleText = title_text;
            holder.poemContent = poem_content_text;
            holder.CreaterNameText = creater_name_text;

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

		// 特定の行のデータを取得
        Poem currentData = getItem(position);

        holder.titleText.setText(currentData.getTitle());
        holder.poemContent.setText(currentData.getContent());
        holder.CreaterNameText.setText("by "+currentData.getUserName());

        return view;
    }
}
