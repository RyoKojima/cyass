package net.cyass.cyass.adapteres;

import java.util.ArrayList;

import net.cyass.cyass.R;
import net.cyass.cyass.models.Category;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CategoryAdapter extends ArrayAdapter<Category> {
	static class ViewHolder {
        TextView categoryNameText;
        TextView categoryHintText;
    }

    private LayoutInflater inflater;

    // コンストラクタ
    public CategoryAdapter(Context context, int resourceId, ArrayList<Category> items) {
        super(context,resourceId, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View view = convertView;

        // Viewを再利用している場合は新たにViewを作らない
        if (view == null) {
            inflater =  (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listitem_category, null);

            TextView category_name_text = (TextView)view.findViewById(R.id.category_name_text);
            TextView category_hint_text = (TextView)view.findViewById(R.id.category_hint_text);

            holder = new ViewHolder();
            holder.categoryNameText = category_name_text;
            holder.categoryHintText = category_hint_text;

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

		// 特定の行のデータを取得
        Category currentData = getItem(position);

        holder.categoryNameText.setText(currentData.getCategoryName());
        holder.categoryHintText.setText(currentData.getCategoryHint());

        return view;
    }
}
