package net.cyass.cyass.utils;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Fragment;
import android.app.FragmentTransaction;

public class MainTabListener implements ActionBar.TabListener {

	private Fragment mFragment;

	public MainTabListener(Fragment fragment) {
		mFragment = fragment;
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		ft.add(android.R.id.content, mFragment, null);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		ft.remove(mFragment);

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}


}
